import 'dart:convert';
import 'dart:io';
import 'package:e_comm_flutter/utils/constants.dart';
import 'package:e_comm_flutter/utils/custom_exception.dart';
import 'package:http/http.dart' as http;

class NetworkService {
  Future<dynamic> get(String path, Map<String, String> headers) async {
    var responseJson;
    try {
      final url = Uri(host: APIURL.baseUrl, path: path);
      final response = await http.get(url, headers: headers);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> post(String path, Map<String, dynamic> params,
      Map<String, String> headers) async {
    var responseJson;
    try {
      final url = Uri(host: APIURL.baseUrl, path: path);
      final body = jsonEncode(params);

      final response = await http.post(url, headers: headers, body: body);
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    print(response.body.toString());
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:
        throw BadRequestException(response.body.toString());
      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:
      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
