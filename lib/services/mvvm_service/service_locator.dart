import 'package:e_comm_flutter/view_models/home_screen_viewmodel.dart';
import 'package:get_it/get_it.dart';

GetIt serviceLocator = GetIt.instance;

void setupServiceLocator() {
  //Services should be register as lazy singleton. Services are shared across app

  //Viewmodles should be register as factory. Viewmodel instances are binded to its view.
  serviceLocator
      .registerFactory<HomeScreenViewModel>(() => HomeScreenViewModel());
}
