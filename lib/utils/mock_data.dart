import 'package:e_comm_flutter/models/app_banner.dart';
import 'package:e_comm_flutter/models/product.dart';

// Return list of mock app banners
// TODO: This file should be converted as load from json
List<AppBanner> dummyAppBanners() {
  return [
    AppBanner(imageUrl: "https://tinyurl.com/y379jw6s"),
    AppBanner(imageUrl: "https://tinyurl.com/y3pjtea4"),
    AppBanner(imageUrl: "https://tinyurl.com/y2gersqn"),
    AppBanner(imageUrl: "https://tinyurl.com/y3c6ksu5"),
    AppBanner(imageUrl: "https://tinyurl.com/y4k2klen"),
    AppBanner(imageUrl: "https://tinyurl.com/y3pccdrc"),
    AppBanner(imageUrl: "https://tinyurl.com/y26fn9rm"),
  ];
}

// Return list of mock recommended products
// TODO: This file should be converted as load from json
List<Product> dummyRecommendedProducts() {
  return [
    Product(imageUrl: "https://tinyurl.com/y4f5e96j"),
    Product(imageUrl: "https://tinyurl.com/y2szwrys"),
    Product(imageUrl: "https://tinyurl.com/y4bfj5b7"),
    Product(imageUrl: "https://tinyurl.com/y44marw5"),
    Product(imageUrl: "https://tinyurl.com/y4urobx8"),
    Product(imageUrl: "https://tinyurl.com/y2yhf95n"),
    Product(imageUrl: "https://tinyurl.com/y5n467o3"),
    Product(imageUrl: "https://tinyurl.com/yxupqdll"),
  ];
}

// Return list of mock caousel proudcts
// TODO: This file should be converted as load from json
List<Product> dummyCarouselProducts() {
  return [
    Product(imageUrl: "https://tinyurl.com/y3w8oaah"),
    Product(imageUrl: "https://tinyurl.com/y4vaulog"),
    Product(imageUrl: "https://tinyurl.com/y3j7rq6g"),
    Product(imageUrl: "https://tinyurl.com/y28jpmyr"),
    Product(imageUrl: "https://tinyurl.com/y2w7fbdo"),
    Product(imageUrl: "https://tinyurl.com/yy2f6lha"),
  ];
}
