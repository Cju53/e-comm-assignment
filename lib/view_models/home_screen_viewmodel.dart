import 'package:e_comm_flutter/models/app_banner.dart';
import 'package:e_comm_flutter/models/product.dart';
import 'package:e_comm_flutter/utils/mock_data.dart';
import 'package:flutter/foundation.dart';

class HomeScreenViewModel extends ChangeNotifier {
  List<AppBanner> banners = [];
  List<Product> recommendedProducts = [];
  List<Product> carouselProducts = [];

  void loadData() async {
    _fetchBanners();
    _fetchRecommendedProducts();
    _fetchCarouselProducts();
  }

  // Function to fetch app banners
  void _fetchBanners() {
    //right now load from mock
    banners = dummyAppBanners();
    notifyListeners();
  }

  // Function to fetch products
  void _fetchRecommendedProducts() {
    //right now load from mock
    recommendedProducts = dummyRecommendedProducts();
    notifyListeners();
  }

  // Function to fetch carousel products
  void _fetchCarouselProducts() {
    //right now load from mock
    carouselProducts = dummyCarouselProducts();
    notifyListeners();
  }
}
