import 'package:e_comm_flutter/common_widgets/product_display_view.dart';
import 'package:e_comm_flutter/models/product.dart';
import 'package:flutter/material.dart';

class ProductCollectionView extends StatelessWidget {
  ProductCollectionView(
      {this.title, this.crossAxisCount = 2, @required this.products});

  final String title;
  final int crossAxisCount;
  final List<Product> products;

  @override
  Widget build(BuildContext context) {
    return Column(children: [_headerView(), _productListGridView()]);
  }

  Widget _headerView() {
    return Container(
      height: 50,
      child: Row(
        children: [Text(title ?? "-")],
      ),
    );
  }

  Widget _productListGridView() {
    return Flexible(
      child: GridView.count(
          primary: false,
          padding: const EdgeInsets.all(20),
          crossAxisSpacing: 10,
          mainAxisSpacing: 10,
          crossAxisCount: crossAxisCount,
          children: products
              .map((product) => ProductDisplayView(product: product))
              .toList()),
    );
  }
}
