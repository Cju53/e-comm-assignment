import 'package:e_comm_flutter/common_widgets/banner_carousel.dart';
import 'package:e_comm_flutter/common_widgets/product_carousel.dart';
import 'package:e_comm_flutter/common_widgets/product_display_view.dart';
import 'package:e_comm_flutter/services/mvvm_service/service_locator.dart';
import 'package:e_comm_flutter/view_models/home_screen_viewmodel.dart';
import 'package:e_comm_flutter/views/home_screen/product_collection_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final HomeScreenViewModel _viewModel = serviceLocator<HomeScreenViewModel>();

  @override
  void initState() {
    _viewModel.loadData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(child: _homeScreenContentBody()));
  }

  Widget _homeScreenContentBody() {
    return ChangeNotifierProvider<HomeScreenViewModel>(
      create: (context) => _viewModel,
      child: Consumer<HomeScreenViewModel>(
        builder: (context, model, child) {
          return Column(children: [
            Container(
                width: double.infinity,
                color: Colors.blue,
                height: 400,
                child: ProductCarousel(
                  products: _viewModel.carouselProducts,
                )),
            Container(
              width: double.infinity,
              height: 400,
              color: Colors.green,
              child: BannerCarousel(
                banners: _viewModel.banners,
              ),
            ),
            Container(
              width: double.infinity,
              height: 800,
              child: ProductCollectionView(
                  products: _viewModel.recommendedProducts,
                  crossAxisCount: 3,
                  title: "Recommended Products"),
            )
          ]);
        },
      ),
    );
  }
}
