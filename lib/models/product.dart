import 'package:json_annotation/json_annotation.dart';
part 'product.g.dart';

@JsonSerializable()
class Product {
  final int id;
  final double price;
  final double offerPrice;
  final String imageUrl;
  final String category;

  Product({this.id, this.price, this.offerPrice, this.imageUrl, this.category});
  factory Product.fromJson(Map<String, dynamic> json) =>
      _$ProductFromJson(json);
  Map<String, dynamic> toJson() => _$ProductToJson(this);
}
