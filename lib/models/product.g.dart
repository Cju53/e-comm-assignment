// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product(
    id: json['id'] as int,
    price: (json['price'] as num)?.toDouble(),
    offerPrice: (json['offerPrice'] as num)?.toDouble(),
    imageUrl: json['imageUrl'] as String,
    category: json['category'] as String,
  );
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'price': instance.price,
      'offerPrice': instance.offerPrice,
      'imageUrl': instance.imageUrl,
      'category': instance.category,
    };
