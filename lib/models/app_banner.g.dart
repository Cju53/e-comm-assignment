// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_banner.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppBanner _$AppBannerFromJson(Map<String, dynamic> json) {
  return AppBanner(
    id: json['id'] as int,
    imageUrl: json['imageUrl'] as String,
  );
}

Map<String, dynamic> _$AppBannerToJson(AppBanner instance) => <String, dynamic>{
      'id': instance.id,
      'imageUrl': instance.imageUrl,
    };
