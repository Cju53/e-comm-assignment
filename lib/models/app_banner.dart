import 'package:json_annotation/json_annotation.dart';
part 'app_banner.g.dart';

@JsonSerializable()
class AppBanner {
  final int id;
  final String imageUrl;

  AppBanner({this.id, this.imageUrl});

  factory AppBanner.fromJson(Map<String, dynamic> json) =>
      _$AppBannerFromJson(json);
  Map<String, dynamic> toJson() => _$AppBannerToJson(this);
}
