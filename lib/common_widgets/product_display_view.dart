import 'package:e_comm_flutter/models/product.dart';
import 'package:flutter/material.dart';

class ProductDisplayView extends StatelessWidget {
  const ProductDisplayView({Key key, @required this.product}) : super(key: key);

  final Product product;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100,
      child: Image.network(product.imageUrl),
    );
  }
}
